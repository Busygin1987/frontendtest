import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`    
    * {
        margin: 0;
        padding: 0; 
    }
    
	body {
		box-sizing: border-box;
		margin: 0;
		padding: 0;
		/* width: 1440px; */
		width: 100%;
		height: auto;
		position: absolute;
		top: 0;
		left: 0;
	}

	#root {
		height: 100%;
		width: 100%;
	}
`;
