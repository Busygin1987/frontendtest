export default async function (fetchSuccess, fetchFailure) {
    try {
        const res = await fetch('https://api.myjson.com/bins/5z1rq', {
            method: "get",
            headers: {
                "Content-Type": "application/json"
            }
        });
        const data = await res.json();
        if (data && data.length > 0) {
            fetchSuccess(data);
        }
    } catch (error) {
        fetchFailure();
    }
}
