export default async function (callback) {
    try {
        const res = await fetch('https://api.exchangeratesapi.io/latest', {
            method: "get"
        });
        const course = await res.json();
        if (course.rates.USD) {
            callback(course.rates.USD);
        } else {
            callback(0);
        }
    } catch (error) {
        console.error(error);
    }
}