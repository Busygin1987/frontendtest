import React from 'react';
import { StoreContext } from '../App';
import RowTitle from '../components/RowTitle';
import RowUserItem from '../components/RowUserItem';
import {
    Title,
    Wrapper,
    ButtonRightPosition as Button,
    Text,
    Cell,
    Info
} from '../styled';
import PopUp from '../components/PopUp';
import getCourseData from './functions/getCourseData.js';
import getUsersData from './functions/getUsersData.js';
import {
    fetchUsers,
    fetchSuccess,
    fetchFailure,
    fetchCourse,
    deleteUser,
    showPopUp,
    hidePopUp,
    selectUser,
    selectAllUsers,
    updateSalary
} from '../actions.js';
import styled from 'styled-components';

const Header = styled.thead`
    background-color: #f0f0f0;
`;

const WrapTable = styled.table`
    width: 1200px;
    /* height: auto; */
    border-radius: 4px;
    box-shadow: 0 12px 24px -12px rgba(0, 0, 0, 0.24);
    background-color: #ffffff;
    margin-bottom: 24px;
    border-collapse: collapse;
    table-layout: fixed;
`;

const Caption = styled.caption`
    margin-bottom: 24px;
    text-align: start;
`;

const Body = styled.tbody`
    text-align: center;
`;

const Table = () => {
    const { state, dispatch } = React.useContext(StoreContext);

    const fetchSuccessHandle = data => {
        dispatch(fetchSuccess(data));
    }

    const fetchFailureHandle = () => {
        dispatch(fetchFailure());
    }

    const fetchCourseHandle = (data) => {
        dispatch(fetchCourse(data));
    }

    const deleteUserHandle = (data) => {
        dispatch(deleteUser(data));
    }

    const showPopUpHandle = (data) => {
        dispatch(showPopUp(data));
    }

    const hidePopUpHandle = () => {
        dispatch(hidePopUp());
    }

    const selectUserHandle = (data) => {
        dispatch(selectUser(data));
    }

    const selectUsersHandle = () => {
        dispatch(selectAllUsers());
    }

    const updateSalaryHandle = (data) => {
        dispatch(updateSalary(data))
    }

    React.useEffect(() => {
      (() => {
            dispatch(fetchUsers());
            getUsersData(fetchSuccessHandle, fetchFailureHandle);
            getCourseData(fetchCourseHandle);
        })();
    }, []);

    return (
        <>
            <Wrapper>
                <WrapTable>
                    <Caption><Title fs={24}>Таблица пользователей</Title></Caption>
                    <Header>
                        <RowTitle selectUsers={selectUsersHandle} />
                    </Header>
                    <>
                        {state.isFetching ? (
                            <Cell as={'td'} ta={'center'} colSpan={12}><Info>Загрузка...</Info></Cell>
                        ) : state.hasError ? (
                            <Cell as={'td'} ta={'center'} colSpan={12}><Info>ПРОИЗОШЛА ОШИБКА</Info></Cell>
                        ) : (
                            <Body>
                                {state.users.length > 0 && state.users.map((item, index, arr) => {
                                    return <React.Fragment key={index}>
                                        <RowUserItem
                                            index={index}
                                            data={item}
                                            deleteUser={deleteUserHandle}
                                            showPopUp={showPopUpHandle}
                                            isCheckedAll={state.isCheckedAll}
                                            selectUser={selectUserHandle}
                                            course={state.course}
                                        />
                                    </React.Fragment>
                                })}
                            </Body>
                        )}
                    </>
                </WrapTable>
                <Button
                    width={222}
                    active={state.isChecked.length !== 0 || state.isCheckedAll}
                    disabled={state.isChecked.length === 0 && !state.isCheckedAll}
                >
                    <Text fw={'bold'} color={'white'}>
                        Удалить выбранные
                    </Text>
                </Button>
            </Wrapper>
            {state.popUp.show &&
                <PopUp
                    hidePopUp={hidePopUpHandle}
                    firstName={state.popUp.user.firstName}
                    lastName={state.popUp.user.lastName}
                    index={state.popUp.index}
                    updateSalary={updateSalaryHandle}
                />} 
        </>
    );
}

export default Table;