import React from 'react';
import { StoreContext } from '../App';
import RowTitle from '../components/RowTitle';
import RowUserItem from '../components/RowUserItem';
import {
    WrapTable,
    Title,
    Col,
    Wrapper,
    Header,
    ButtonRightPosition as Button,
    Text,
    Line,
    Info
} from '../styled';
import PopUp from '../components/PopUp';
import getCourseData from './functions/getCourseData.js';
import getUsersData from './functions/getUsersData.js';
import {
    fetchUsers,
    fetchSuccess,
    fetchFailure,
    fetchCourse,
    deleteUser,
    showPopUp,
    hidePopUp,
    selectUser,
    selectAllUsers,
    updateSalary
} from '../actions.js';

const Table = () => {
    const { state, dispatch } = React.useContext(StoreContext);

    const fetchSuccessHandle = data => {
        dispatch(fetchSuccess(data));
    }

    const fetchFailureHandle = () => {
        dispatch(fetchFailure());
    }

    const fetchCourseHandle = (data) => {
        dispatch(fetchCourse(data));
    }

    const deleteUserHandle = (data) => {
        dispatch(deleteUser(data));
    }

    const showPopUpHandle = (data) => {
        dispatch(showPopUp(data));
    }

    const hidePopUpHandle = () => {
        dispatch(hidePopUp());
    }

    const selectUserHandle = (data) => {
        dispatch(selectUser(data));
    }

    const selectUsersHandle = () => {
        dispatch(selectAllUsers());
    }

    const updateSalaryHandle = (data) => {
        dispatch(updateSalary(data))
    }

    React.useEffect(() => {
      (() => {
            dispatch(fetchUsers());
            getUsersData(fetchSuccessHandle, fetchFailureHandle);
            getCourseData(fetchCourseHandle);
        })();
    }, []);

    return (
        <>
            <Wrapper>
                <Col mb={25}>
                    <Title fs={24}>Таблица пользователей</Title>
                </Col>
                <WrapTable>
                    <Header>
                        <RowTitle selectUsers={selectUsersHandle} />
                    </Header>
                    {state.isFetching ? (
                        <Info>Загрузка...</Info>
                    ) : state.hasError ? (
                        <Info>ПРОИЗОШЛА ОШИБКА</Info>
                    ) : (
                        <>
                            {state.users.length > 0 && state.users.map((item, index, arr) => {
                                return <React.Fragment key={index}>
                                    <RowUserItem
                                        index={index}
                                        data={item}
                                        deleteUser={deleteUserHandle}
                                        showPopUp={showPopUpHandle}
                                        isCheckedAll={state.isCheckedAll}
                                        selectUser={selectUserHandle}
                                        course={state.course}
                                    />
                                    {index !== arr.length - 1 && <Line />}
                                </React.Fragment>
                            })}
                        </>
                    )}
                </WrapTable>
                <Button
                    width={222}
                    active={state.isChecked.length !== 0 || state.isCheckedAll}
                    disabled={state.isChecked.length === 0 && !state.isCheckedAll}
                >
                    <Text fw={'bold'} color={'white'}>
                        Удалить выбранные
                    </Text>
                </Button>
            </Wrapper>
            {state.popUp.show &&
                <PopUp
                    hidePopUp={hidePopUpHandle}
                    firstName={state.popUp.user.firstName}
                    lastName={state.popUp.user.lastName}
                    index={state.popUp.index}
                    updateSalary={updateSalaryHandle}
                />} 
        </>
    );
}

export default Table;