import React from 'react';
import Checkbox from './Checkbox';
import { Title, Cell, Row } from '../styled';

const  RowTitle = ({
    selectUsers = () => {}
}) => {
  return (
    <Row height={47}>
        <Cell pl={24} pt={4} colSpan={1}>
            <Checkbox selectUsers={selectUsers} />
        </Cell>
        <Cell colSpan={1}>
            <Title>№</Title>
        </Cell>
        <Cell colSpan={3} pl={10}>
            <Title>ФИО</Title>
        </Cell>
        <Cell colSpan={2}>
            <Title>Возраст (Лет)</Title>
        </Cell>
        <Cell colSpan={1}>
            <Title>Рост</Title>
        </Cell>
        <Cell colSpan={1}>
            <Title>Вес</Title>
        </Cell>
        <Cell colSpan={3} pr={24}>
            <Title>Зарплата</Title>
        </Cell>
    </Row>
  );
};

export default RowTitle;