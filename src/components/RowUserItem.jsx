import React from 'react';
import Checkbox from './Checkbox';
import { Text, Cell, Row } from '../styled';
import Image from './Image';
import convertHeight from './functions/convertHeight.js';
import getYearOfBirth from './functions/getYearOfBirth.js';
import deleteIcon from '../images/delete.svg';
import updateIcon from '../images/update.svg';

const  RowTitle = ({
    index,
    data: {
        firstName,
        lastName,
        height,
        dateOfBirth,
        salary,
        weight
    },
    deleteUser = () => {},
    showPopUp = () => {},
    selectUser = () => {},
    isCheckedAll,
    course
}) => {
    const [ modified, setModified ] = React.useState(false);
    return (
        <Row border height={60}>
            <Cell as={'td'} pt={4} pl={24} colSpan={1}>
                <Checkbox
                    index={index}
                    isCheckedAll={isCheckedAll}
                    selectUser={selectUser}
                />
            </Cell>
            <Cell as={'td'} colSpan={1}>
                <Text>{index+1}</Text>
            </Cell>
            <Cell as={'td'} colSpan={3} pl={10}>
                <Text>{`${firstName} ${lastName}`}</Text>
            </Cell>
            <Cell as={'td'} colSpan={2}>
                <Text>{getYearOfBirth(dateOfBirth)}</Text>
            </Cell>
            <Cell as={'td'} colSpan={1}>
                <Text>{convertHeight(height)}</Text>
            </Cell>
            <Cell as={'td'} colSpan={1}>
                <Text>{weight && `${Math.round(weight/2.205)} кг`}</Text>
            </Cell>
            <Cell as={'td'} colSpan={3}>
                <Text>{modified ? `$${Math.round(salary)}` : ( `$${Math.round(salary * course)}`, setModified(true) )}</Text>
                <div style={{
                    display: 'inline-block',
                    position: 'absolute',
                    right: '24px'
                }}>
                    <Image
                        bg={updateIcon}
                        width={14}
                        height={14}
                        onClick={() => {
                            showPopUp(index);
                        }}
                    />
                    <Image
                        ml={24}
                        bg={deleteIcon}
                        width={16}
                        height={16}
                        onClick={() => {
                            deleteUser(index);
                        }}
                    />
                </div>
            </Cell>
        </Row>
    );
};

export default RowTitle;