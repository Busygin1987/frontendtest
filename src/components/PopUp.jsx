import React from 'react';
import {
    Title,
    Text,
    StyledText,
    PopUp,
    PopUpWrap,
    Block,
    Input,
    CircleClose,
    Button
} from '../styled';

const PopUpComponent = ({
    hidePopUp = () => {},
    updateSalary = () => {},
    firstName,
    lastName,
    index
}) => {
    const [ salary, setSalary] = React.useState('');
    return (
        <PopUpWrap>
            <PopUp>
                <Title fs={24}>Редактирование зарплаты</Title>
                <Block>
                    <Text fw={'bold'}>Получатель:</Text>
                    <StyledText>{`${firstName} ${lastName}`}</StyledText>
                </Block>
                <Input
                    value={salary}
                    onChange={(event) => {
                        if (/^\d+$/.test(event.target.value) || event.target.value === '') {
                            setSalary(event.target.value);
                        }
                    }}
                />
                <Button
                    width={222}
                    onClick={() => {
                        if (salary > 0) {
                            updateSalary({ salary, index });
                            hidePopUp();
                        }
                    }}
                >
                    <Text fw={'bold'} color={'white'}>
                        Изменить зарплату
                    </Text>
                </Button>
                <CircleClose
                    onClick={() => {
                        hidePopUp();
                    }}
                />
            </PopUp>
        </PopUpWrap>
    )
}

export default PopUpComponent;