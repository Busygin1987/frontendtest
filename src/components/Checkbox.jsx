import React from 'react';
import styled from 'styled-components';
import chevron from '../images/chevron.svg';

const Checkbox = styled.input`
    -webkit-appearance: none;
    -moz-appearance:    none;
    appearance:         none;
    width: 24px;
    height: 24px;
    border-radius: 4px;
    border: solid 2px rgba(198, 198, 198, .5);
    background-color: #ffffff;
    cursor: pointer;
    outline: none;

    &:checked {
        background-color: #f5faff;
        border-color:  #2988ff;
        background-image: url(${chevron});
        background-repeat: no-repeat;
        background-position: center;
    }
`;


const CheckboxComponent = ({ 
    index,
    isCheckedAll,
    selectUser = () => {},
    selectUsers = () => {}
 }) => {
    const [ checked, setChecked ] = React.useState(false);

    return (
        <Checkbox
            checked={isCheckedAll || checked}
            readOnly
            type={'checkbox'}
            onClick={() => {
                setChecked(!checked);
                if (index >= 0) {
                    selectUser(index);                   
                } else {
                    selectUsers();
                }
            }}
        />
    )
}

export default CheckboxComponent;