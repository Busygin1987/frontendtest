export default function (date) {
    return new Date().getFullYear() - new Date(date * 1000).getFullYear()
}