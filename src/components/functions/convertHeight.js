export default function (height) {
    if (height && typeof height === 'string') {
        let getInchLength;
        const res = height.split(`'`);
        const getFootLength = (res[0]/3.2808) * 100;
        if (res[1]) {
            getInchLength = res[1].replace(`"`, '') * 2.54;
        } else {
            getInchLength = 0
        }
        const meters = String(Math.round(getFootLength + getInchLength)/100).split('.');
        return `${meters[0]}м ${meters[1]}см`
    } else {
        return ''
    }
}