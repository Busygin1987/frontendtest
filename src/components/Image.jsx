import React from 'react';
import styled from 'styled-components';

const Image = styled.div`
	width: ${({ width }) => width ? width + 'px' : '100%'};
    margin-left: ${({ ml }) => ml && ml + 'px'};
	height: ${({ height }) => height ? height + 'px' : '100%'};
	${({ bg }) => bg ? `background-image: url(${bg});` :  ``};
	background-size: ${({ size }) => size ? size : 'contain'};
	background-repeat: no-repeat;
	background-position: center;
    display: inline-block;
`;

const ImageComponent = ({ onClick = () => {}, bg, height, width, ml }) => {
    return (
        <Image 
            onClick={onClick}
            bg={bg}
            height={height}
            width={width}
            ml={ml}
        />
    )
}

export default ImageComponent;