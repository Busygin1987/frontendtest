import { actionTypes } from './actions';

export const initialState = {
    isFetching: false,
    users: [],
    isCheckedAll: false,
    isChecked: [],
    hasError: false,
    course: 0,
    popUp: {
        show: false,
        user: {},
        index: null
    },
};


export const reducer = (state, action) => {
    Object.freeze(state);
    switch (action.type) {
        case actionTypes.FETCH_USERS:
            return {
                ...state,
                isFetching: true
            };
        case actionTypes.FETCH_USERS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                users: action.payload,
            };
        case actionTypes.FETCH_USERS_FAILURE:
            return {
                ...state,
                hasError: true,
                isFetching: false
            };
        case "DELETE_ALL_USERS":
            return {
                ...state,
                isLoaded: false
            };
        case actionTypes.DELETE_USER:
            let users = state.users.slice();
            users.splice(action.payload, 1);
            return {
                ...state,
                users,
            };
        case actionTypes.SELECT_USER: 
            if (state.isChecked.includes(action.payload)) {
                state.isChecked.splice(state.isChecked.indexOf(action.payload), 1);
            } else {
                state.isChecked.push(action.payload);
            }
            return {
                ...state
            }
        case actionTypes.SELECT_ALL_USERS: 
            return {
                ...state,
                isCheckedAll: !state.isCheckedAll
            }
        case actionTypes.UPDATE_USER_SALARY:
            state.users[action.payload.index].salary = action.payload.salary;
            return {
                ...state
            };
        case actionTypes.FETCH_COURSE:
            return {
                ...state,
                course: action.payload,
            };
        case actionTypes.SHOW_POPUP:
            const index = action.payload;
            let user;
            if (index >= 0) {
                const users = state.users.slice();
                user = users.find((item, i) => index === i);
            }
            return {
                ...state,
                popUp: {
                    show: true,
                    user,
                    index
                },
            };
        case actionTypes.HIDE_POPUP:
            return {
                ...state,
                popUp: {
                    show: false
                },
            };
    default:
        return state;
    }
};