export const actionTypes = {
    FETCH_USERS: 'FETCH_USERS',
    FETCH_USERS_SUCCESS: 'FETCH_USERS_SUCCESS',
    FETCH_USERS_FAILURE: 'FETCH_USERS_FAILURE',
    DELETE_ALL_USERS: 'DELETE_ALL_USERS',
    DELETE_USER: 'DELETE_USER',
    SELECT_USER: 'SELECT_USER',
    SELECT_ALL_USERS: 'SELECT_ALL_USERS',
    UPDATE_USER_SALARY: 'UPDATE_USER_SALARY',
    FETCH_COURSE: 'FETCH_COURSE',
    SHOW_POPUP: 'SHOW_POPUP',
    HIDE_POPUP: 'HIDE_POPUP'
};

export const fetchUsers = () => ({
    type: actionTypes.FETCH_USERS,
});

export const fetchSuccess = (data) => ({
    type: actionTypes.FETCH_USERS_SUCCESS,
    payload: data
});

export const fetchFailure = () => ({
    type: actionTypes.FETCH_USERS_FAILURE
});

export const fetchCourse = (data) => ({
    type: actionTypes.FETCH_COURSE,
    payload: data
});

export const deleteUser = (index) => ({
    type: actionTypes.DELETE_USER,
    payload: index
});

export const showPopUp = (index) => ({
    type: actionTypes.SHOW_POPUP,
    payload: index
});

export const hidePopUp = () => ({
    type: actionTypes.HIDE_POPUP
});

export const selectUser = (data) => ({
    type: actionTypes.SELECT_USER,
    payload: data
});

export const selectAllUsers = () => ({
    type: actionTypes.SELECT_ALL_USERS
});

export const updateSalary = (data) => ({
    type: actionTypes.UPDATE_USER_SALARY,
    payload: data
});