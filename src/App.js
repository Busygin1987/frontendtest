import React from 'react';
import { reducer, initialState } from './reducer.js';
import Table from './modules/new-Table';
import { WrapAppPage } from './styled';
import GlobalStyles from './globalStyles.js';

export const StoreContext = React.createContext();

function App() {
    const [ state, dispatch ] = React.useReducer(reducer, initialState);

    return (
        <StoreContext.Provider value={{ state, dispatch }}>
            <WrapAppPage>
                <Table />
            </WrapAppPage>
            <GlobalStyles/>
        </StoreContext.Provider>
    );
}

export default App;
