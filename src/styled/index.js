import styled from 'styled-components';
import cross from '../images/cross.svg';

export const WrapAppPage = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    width: auto;
    height: auto;
    background-color: #f5f5f5;
    padding: 0;
    cursor: pointer;
`;

export const WrapTable = styled.div`
    width: 1200px;
    height: auto;
    border-radius: 4px;
    box-shadow: 0 12px 24px -12px rgba(0, 0, 0, 0.24);
    background-color: #ffffff;
    margin-bottom: 24px;
`;

export const Title = styled.span`
    font-family: Verdana;
    font-size: ${(prop) => prop.fs ? prop.fs + 'px' : '14px'};
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: 0.4px;
    color: #000;
`;

export const Text = styled.span`
    font-family: Verdana;
    font-size: 14px;
    font-weight: ${(prop) => prop.fw ? prop.fw : 'normal'};
    font-stretch: normal;
    font-style: normal;
    line-height: 1.29;
    letter-spacing: 0.4px;
    color: ${(prop) => prop.color === 'white' ? '#ffffff' : '#000000' };
`;

export const Wrap = styled.div`
    min-height: ${(prop) => prop.height ? prop.height + 'px' : '47px'};
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-flow: row wrap;
    padding-left: 24px;
    padding-right: 24px;
`;

export const Col = styled.div`
    box-sizing: border-box;
    flex-grow: ${(prop) => !prop.grow ? 1 : 0};
    width: ${(prop) => prop.w ? prop.w + 'px' : ''};
    margin-left: ${(prop) => prop.ml ? prop.ml + 'px' : ''};
    margin-right: ${(prop) => prop.mr ? prop.mr + 'px' : ''};
    margin-top: ${(prop) => prop.mt ? prop.mt + 'px' : ''};
    margin-bottom: ${(prop) => prop.mb ? prop.mb + 'px' : ''};
`;

export const Wrapper = styled.div`
    margin-right: auto;
    margin-left: auto;
    margin-top: 90px;
    margin-bottom: 230px;
    position: relative;
`;

export const Header = styled.div`
    height: 47px;
    background-color: #f0f0f0;
    border-radius: 4px;
`;

export const PopUpWrap = styled.div`
    width: auto;
    height: auto;
    -webkit-backdrop-filter: blur(10px);
    backdrop-filter: blur(10px);
    background-color: rgba(0, 0, 0, 0.3);
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const PopUp = styled.div`
    padding: 36px 24px;
    width: 432px;
    height: 342px;
    border-radius: 4px;
    box-shadow: 0 12px 24px -12px rgba(0, 0, 0, 0.24);
    background-color: #ffffff;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    box-sizing: border-box;
    position: relative;
`;

export const Block = styled.div`
    width: 100%;
    min-height: 42px;
    text-align: start;
`;

export const StyledText = styled(Text)`
    display: block;
    margin-bottom: 6px;
`;

export const Input = styled.input`
    width: 100%;
    height: 42px;
    border-radius: 4px;
    border: solid 1px #c6c6c6;
    background-color: #ffffff;
    padding-left: 24px;
    box-sizing: border-box;
`;

export const Button = styled.button`
    width: ${(prop) => prop.width ? prop.width + 'px' : '100%'};
    height: 48px;
    border-radius: 4px;
    border: 0;
    box-shadow: 0 12px 18px -6px rgba(0, 59, 133, 0.24), 0 8px 5px -6px rgba(0, 48, 109, 0.43);
    background-color: #2988ff;
    cursor: pointer;
    outline: none;
`;

export const ButtonRightPosition = styled(Button)`
    position: absolute;
    right: 0;
    background-color: ${(prop) => prop.active ? '#2988ff' : '#aaaaaa'};

    &:hover {
        background-color: ${({ active }) => active && `#4e9cff`};
    }

    &:active {
        background-color: ${({ active }) => active && `#1f61b4`};
    }
`;

export const CircleClose = styled.div`
    width: 36px;
    height: 36px;
    background-color: #2988ff;

    background-image: url(${cross});
	background-repeat: no-repeat;
	background-position: center;
    border-radius: 24px;
    box-shadow: 0 12px 18px -6px rgba(0, 59, 133, 0.24), 0 8px 5px -6px rgba(0, 48, 109, 0.43);

    position: absolute;
    left: 433px;
    bottom: 344px;
`;

export const Row = styled.tr`
    min-height: ${({ height }) => height ? height + 'px' : '47px'};
    ${({ border }) => border && `border-bottom: solid 1px #f5f5f5;` }
`;

export const Cell = styled.th`
    text-align: ${(prop) => prop.ta ? prop.ta : 'start'};
    padding-right: ${(prop) => prop.pr ? prop.pr + 'px' : ''}; 
    padding-top: ${(prop) => prop.pt ? prop.pt + 'px' : ''};
    padding-bottom: ${(prop) => prop.pb ? prop.pb + 'px' : ''};
    padding-left: ${(prop) => prop.pl ? prop.pl + 'px' : ''};
`;

export const Info = styled.div`
    align-self: center;
    width: 100%;
    text-align: center;
    height: 48px;
    font-size: 20px;
    font-weight: bold;
    padding-top: 24px;
`;